<?php

	// by LetMeCode.ru

	header('Content-Type: application/javascript');
	
	$domain = isset($_GET['domain']) ? $_GET['domain'] : '';
	$https = isset($_GET['https']) ? true : false;

	$id = isset($_GET['id']) ? $_GET['id'] : 0;
	if ($id == 0 || $domain == '') {
		exit();
	}
	$url = 'http://' . $domain . '/pl/lite/widget/script?id=' . $id;

	$cached = __DIR__ . '/cache/' . md5($url) . $id;
	if (file_exists($cached)) {
		$f = file_get_contents($cached);
	} else {
		$f = file_get_contents($url);
		file_put_contents($cached, $f);
	}
	
	$out = $f;
	$error = false;
	
	$patches = array(
		array(
			'/document\.addEventListener\(\"DOMContentLoaded\"\, function\(\) \{/',
			'window.getcourse_widgets_add(currentScript, function(script) {',
		),
		array(
			'/var script \= document\.getElementById\(\'.+\'\)\;/',
			'',
		),
		array(
			'/var domain \= \( \(getLocation\( currentScript\.src \)\)\.hostname \)\;/',
			'var domain = "' . $domain . '";',
		),
		array(
			'/http\:\/\//',
			'http' . ($https ? 's' : '') . '://',
		),
		array(
			'/var gcEmbedOnMessage \= function\(e\) \{/',
			'var gcEmbedOnMessage = function(e) {
				if (e.source != iframe.contentWindow) return;
				if ( e.data.height ) {
					par.style.height = ( e.data.height ) + "px";
					iframe.style.height = ( e.data.height ) + "px";
				}',
		),
	);
	
	$cur = null; $total = 0;
	function preg_cb($matches) {
		global $cur, $total, $error;
		if ($cur === null) {
			$error = true;
			return $cur;
		}
		$total++;
		$c = $cur;
		$cur = null;
		return $c;
	}
	
	foreach ($patches as $patch) {
		$cur = $patch[1];
		$f = preg_replace_callback($patch[0], 'preg_cb', $f);
		if ($error) {
			break;
		}
	}
	
	if ($total !== count($patches)) {
		$error = true;
	}
	
	if (!$error) {
		$f = '
			if (typeof(window.getcourse_widgets) == "undefined") {
				window.getcourse_widgets = [];
			}
			if (typeof(window.getcourse_trigger) == "undefined") {
				window.getcourse_trigger = function() {
					for (var i = 0; i < getcourse_widgets.length; i++) {
						var f = getcourse_widgets[i][1];
						f(getcourse_widgets[i][0]);
					}
				}
				window.addEventListener("load", function() {
					setTimeout(window.getcourse_trigger, 10);
				});
			}
			var currentScript = document.currentScript || (function() {
				var scripts = document.getElementsByTagName("script");
				return scripts[scripts.length - 1];
			})();
			if (typeof(window.getcourse_widgets_add) == "undefined") {
				window.getcourse_widgets_add = function(s, f) {
					window.getcourse_widgets.push([s, f]);
				};
			}
		' . $f;
		$out = $f;
	}

	echo $out;
