/*
 * 
 * by LetMeCode.ru
 * 
 */

(function($) {
	
	function send_message(data) {
		data.name = 'fokken_getcourse_widget';
		window.parent.postMessage(data, '*');
	}

	$(window).on('message', function(e) {
		var data = e.originalEvent.data;
		if (data) {
			if (data.style) {
				var css = data.style;
				$('head').append('<style type="text/css">' + css + '</style>');
				uptade_height();
			}
			if (data.script) {
				var js = data.script;
				$('head').append('<script>' + js + '</script>');
				uptade_height();
			}
		}
	});
	
	send_message({
		'ready': true,
	});
	
	var old_height = 0;
	function uptade_height() {
		var new_height = $(window.document.body).height();
		if (new_height !== old_height) {
			send_message({
				'height': new_height,
			});
			old_height = new_height;
		}
	}
	
	var css = {
		'global': {
			'.part-button': {
				'position': 'relative',
			},
			'.part-button.loading-show .loading_blob': {
				'display': 'block',
			},
			'.part-button.loading-show button': {
				'visibility': 'hidden',
			},
			'.loading_blob': {
				'display': 'none',
				'width': '2rem',
				'height': '2rem',
				'background': 'rgba(130,130,130,0.85)',
				'border-radius': '50%',
				'position': 'absolute',
				'left': 'calc(50% - 1rem)',
				'top': 'calc(50% - 1rem)',
				'box-shadow': '0 0 1rem rgba(200, 200, 200, 0.25)',
			},
			'.loading_blob2': { 'animation': 'loading_blob2 1.5s infinite' },
			'.loading_blob3': { 'animation': 'loading_blob3 1.5s infinite' },
			'.loading_blob1': { 'animation': 'loading_blob1 1.5s infinite' },
			'.loading_blob4': { 'animation': 'loading_blob4 1.5s infinite' },
			'.loading_blob0': { 'animation': 'loading_blob0 1.5s infinite' },
			'.loading_blob5': { 'animation': 'loading_blob5 1.5s infinite' },
		},
		'@keyframes loading_blob2': {
			'25%, 75%': { 'transform': 'translateX(-1.5rem) scale(0.75)' },
			'95%': { 'transform': 'translateX(0rem) scale(1)' },
		},
		'@keyframes loading_blob3': {
			'25%, 75%': { 'transform': 'translateX(1.5rem) scale(0.75)' },
			'95%': { 'transform': 'translateX(0rem) scale(1)' },
		},
		'@keyframes loading_blob1': {
			'25%': { 'transform': 'translateX(-1.5rem) scale(0.75)' },
			'50%, 75%': { 'transform': 'translateX(-4.5rem) scale(0.6)' },
			'95%': { 'transform': 'translateX(0rem) scale(1)' },
		},
		'@keyframes loading_blob4': {
			'25%': { 'transform': 'translateX(1.5rem) scale(0.75)' },
			'50%, 75%': { 'transform': 'translateX(4.5rem) scale(0.6)' },
			'95%': { 'transform': 'translateX(0rem) scale(1)' },
		},
		'@keyframes loading_blob0': {
			'25%': { 'transform': 'translateX(-1.5rem) scale(0.75)' },
			'50%': { 'transform': 'translateX(-4.5rem) scale(0.6)' },
			'75%': { 'transform': 'translateX(-7.5rem) scale(0.5)' },
			'95%': { 'transform': 'translateX(0rem) scale(1)' },
		},
		'@keyframes loading_blob5': {
			'25%': { 'transform': 'translateX(1.5rem) scale(0.75)' },
			'50%': { 'transform': 'translateX(4.5rem) scale(0.6)' },
			'75%': { 'transform': 'translateX(7.5rem) scale(0.5)' },
			'95%': { 'transform': 'translateX(0rem) scale(1)' },
		},
	};
	
	function get_css() {
		var out = '', o, p, k, l, m;
		for (k in css) { if (css.hasOwnProperty(k)) {
			o = css[k];
			if (k != 'global') out += k + '{';
			for (l in o) { if (o.hasOwnProperty(l)) {
				p = o[l];
				out += l + '{';
				for (m in p) { if (p.hasOwnProperty(m)) {
					out += m + ':' + p[m] + ';';
				}}
				out += '}';
			}}
			if (k != 'global') out += '}';
		}}
		return out;
	}
	
	function catcher_in_the_shit() {
		var d = $('.form-result-block').css('display');
		if (d === 'block') {
			loading_hide();
		} else {
			setTimeout(catcher_in_the_shit, 10);
		}
	}
	
	function loading_show() {
		$('.form-result-block').css('display', 'none');
		$('.part-button').addClass('loading-show');
		uptade_height();
		catcher_in_the_shit();
	}
	
	function loading_hide() {
		$('.part-button').removeClass('loading-show');
		uptade_height();
	}
	
	$('body').on('DOMNodeInserted', function () {
		  uptade_height();
	});
	
	$(function() {
		var style = $('<style>' + get_css() + '</style>');
		$('html > head').append(style);
		
		for (var i = 0; i <= 5; i++) {
			$('.part-button').append('<div class="loading_blob loading_blob' + i + '"></div>');
		}
		
		$('form').on('submit', function() {
			loading_show();
		});
		
		var p = window.location.href.split(':');
		var q = $('form').attr('action').split(':');
		var r = q.length == 1 ? ('//' + window.location.hostname + q[0]) : q[1];
		$('form').attr('action', p[0] + ':' + r);
		
		$('input[type="text"],input[type="email"],input[type="password"]')
			.attr('autocorrect', 'off')
			.attr('autocapitalize', 'off')
			.attr('spellcheck', 'false');
			
		$('#gccounterImg').css('opacity', 0);
		
		$(window).on('resize', function(){
			uptade_height();
		});
		
		uptade_height();
	});
	
})(jQuery);
